/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImageProces;

import static ImageProces.CopiarArchivo.recoverNext;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Geek
 */
public class Imagen {
///////////////////////////////////////////////////////
//          Atributos
//////////////////////////////////////////////////////    
    public static final int anchoPermitido=1024;
    public static final int altoPermitido=1024;
    private Color arreglo[][];
    private double matriz[][];
    private int ancho;
    private int alto;
    private String rutaFichOrigen;
    
///////////////////////////////////////////////////////
//          Metodos
//////////////////////////////////////////////////////   
/********
 * constructor
 * @param archivo 
 */
    public Imagen(String archivo) {
        rutaFichOrigen=archivo;
        arreglo = new Color[anchoPermitido][anchoPermitido];
        cargarImagen(archivo);
        matriz = new double[alto][ancho];
    }
    public void copiarImagen( ){

        File miDir = new File (".");
        try {
            System.out.println ("Directorio actual: " + miDir.getCanonicalPath());
        }catch(Exception e) {
            e.printStackTrace();
        }

        String rutaFichDestino="./copia1.jpg"; //destino
        File ficheroOrigen=new File(rutaFichOrigen);//objeto con la ruta origen.
        File ficheroDestino=new File(rutaFichDestino);//objeto con la ruta destino.
        BufferedInputStream lectorFichero;//flujo buffer de lectura .
        BufferedOutputStream escritorFichero;//buffer de escritura.
        try{
            lectorFichero=new BufferedInputStream(new FileInputStream(ficheroOrigen));//Inicializa el buffer de lectura con un FileInputStream
            escritorFichero=new BufferedOutputStream(new FileOutputStream(ficheroDestino));//Inicializa el buffer de escritura con un FileOutputStream


            //int[] intRead, invInt;
            int[] intRead,intAux;
            intRead = new int[1];
            intAux = new int[1];
            long conv;
            intAux[0]=0;
            intRead[0]=0;
            do{    
            intAux=intRead;
            intRead=recoverNext(1,lectorFichero, escritorFichero);
            
            }while(!(intRead[0]==0xD9&&intAux[0]==0XFF));

            lectorFichero.close();
            escritorFichero.close();

        }
        catch(FileNotFoundException e){
            e.printStackTrace();

        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public void cargarImagen(String archivo){
        BufferedImage bf=null;
        try {
            bf = ImageIO.read(new File(archivo));
        } catch (IOException ex) {
            Logger.getLogger(Imagen.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (bf.getWidth()<anchoPermitido) {
            ancho=bf.getWidth();
        }else
            ancho=anchoPermitido;
        if (bf.getHeight()<altoPermitido) {
            alto=bf.getHeight();
        }else
            alto=altoPermitido;
        int cont=0;
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                cont++;
                arreglo[i][j]= new Color(bf.getRGB(j, i));
                //System.out.println(cont +":"+"RedGreenBlue:"+ bf.getRGB(j, i));
            }
        }
    }
    public void binarizarImagen(double umbral){
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                //Color pix= arreglo[i][j];
                //int promedio =(int)Math.sqrt(Math.pow(pix.getBlue(),2)+Math.pow(pix.getRed(),2)+Math.pow(pix.getGreen(),2));;
                double m=matriz[i][j];
                if (m<umbral) 
                    arreglo[i][j]=Color.BLACK;
                else
                    arreglo[i][j] = Color.WHITE;
            }
        }
    }
    public void binarizarImagenBN(){
        this.matrizDepromB();
        double umbral=this.parametroT();
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                //Color pix= arreglo[i][j];
                //int promedio =(int)Math.sqrt(Math.pow(pix.getBlue(),2)+Math.pow(pix.getRed(),2)+Math.pow(pix.getGreen(),2));;
                double m=matriz[i][j];
                if (m<umbral) 
                    arreglo[i][j]=Color.BLACK;
 
                else
                    arreglo[i][j] = Color.WHITE;
 
            }
        }
    }
    public void binarizarImagenAR(){
        this.matrizDepromA();
        double umbral=this.parametroT();
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                //Color pix= arreglo[i][j];
                //int promedio =(int)Math.sqrt(Math.pow(pix.getBlue(),2)+Math.pow(pix.getRed(),2)+Math.pow(pix.getGreen(),2));;
                double m=matriz[i][j];
                if (m<umbral) 
                    //arreglo[i][j]=Color.BLACK;
                    arreglo[i][j]=Color.BLUE;
                else
                    //arreglo[i][j] = Color.WHITE;
                    arreglo[i][j]=Color.RED;
            }
        }
    }
    public void filtroSepia(){
        
        int rojo=0;
        int verde=0;
        int azul=0;
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                Color pix= arreglo[i][j];
                rojo=(int)pix.getRed();
                verde=(int)pix.getGreen();
                azul=(int)pix.getBlue();
                
                int nr=(int)(0.393*rojo+0.769*verde+0.189*azul);
                int nv=(int)(0.349*rojo+0.686*verde+0.168*azul);
                int na=(int)(0.272*rojo+0.534*verde+0.131*azul);
                
                if(nr>255)
                    rojo=255;
                else 
                    rojo=nr;
                
                if(nv>255)
                    verde=255;
                else 
                    verde=nv;
                
                if(na>255)
                    azul=255;
                else 
                    azul=na;
                
                arreglo[i][j]=new Color(rojo,verde,azul);
            }
        }
    }
    public BufferedImage imprimirImagen(){
        BufferedImage salida = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                salida.setRGB(j, i, arreglo[i][j].getRGB());
            }
        }
        return salida;
    }
    private void matrizDepromB(){
        //matriz = new double[alto][ancho];
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                Color pix= arreglo[i][j];
                 //matriz[i][j] =Math.sqrt(Math.pow(pix.getBlue(),2)+Math.pow(pix.getRed(),2)+Math.pow(pix.getGreen(),2));
                matriz[i][j]=(pix.getBlue()+pix.getRed()+pix.getGreen())/Math.sqrt(3);

            }
        }
    
        
    }
    private void matrizDepromA(){
        //matriz = new double[alto][ancho];
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                Color pix= arreglo[i][j];
                 //matriz[i][j] =Math.sqrt(Math.pow(pix.getBlue(),2)+Math.pow(pix.getRed(),2)+Math.pow(pix.getGreen(),2));
                //matriz[i][j]=(pix.getBlue()+pix.getRed()+pix.getGreen())/Math.sqrt(3);
                //matriz[i][j]=Math.abs(pix.getBlue()-pix.getRed())/Math.sqrt(2);
                matriz[i][j]=Math.abs(pix.getRed()-pix.getBlue())/Math.sqrt(3);
            }
        }
    
        
    }
    
    private double parametroT(){
        double t=0;
        for (int i = 0; i < alto; i++) {
            for (int j = 0; j < ancho; j++) {
                t=t+matriz[i][j];
                
            }
        }
        t=t/(alto*ancho);
        System.out.println(t);
        return t;    
    }
    static int[] recoverNext(int nextBytes,BufferedInputStream lectorFichero, BufferedOutputStream escritorFichero){
        int[] bytesRecovered= new int[nextBytes];
        String byteRead="";
        int count=0;
        int byteRecovered;
        try{
            if(nextBytes>0){
                while(count<nextBytes){
                    if((byteRecovered=lectorFichero.read())!=-1){
                       //System.out.println("Leido: "+byteRecovered);

                       bytesRecovered[count]=byteRecovered;
                       writeToFile(escritorFichero, byteRecovered);
                    }
                    count++;
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return(bytesRecovered);
    }

    static int[] reverseInt(int[] bytes){
        int[] revString= new int[bytes.length];
        int index=0;
        for (int x=bytes.length-1;x>=0;x--){
            revString[index++] = bytes[x];
        }
        return(revString);
    }

    static void printArrayInt(int[] intArray){
        for (int x=0; x<intArray.length;x++){
            System.out.print(intArray[x]+" ");
        }
    }

    static void printArrayIntInHex(int[] intArray){
        for (int x=0; x<intArray.length;x++){
            System.out.print(Integer.toHexString(intArray[x])+" ");
        }
    }

    static long StringToInt(String bytes){
        long value=0;

        value=Long.parseLong(bytes, 16);

        return(value);
    }

    static void writeToFile(BufferedOutputStream escritorFichero, int bytes){
        try{
            escritorFichero.write(bytes);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
