/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ImageProces;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopiarArchivo {

    /**
     * @param args the command line arguments
     
    public static void main(String[] args) {

        File miDir = new File (".");
        try {
            System.out.println ("Directorio actual: " + miDir.getCanonicalPath());
        }catch(Exception e) {
            e.printStackTrace();
        }


        String rutaFichOrigen="./entrada.jpg";//origen
        String rutaFichDestino="./copia1.jpg"; //destino
        File ficheroOrigen=new File(rutaFichOrigen);//objeto con la ruta origen.
        File ficheroDestino=new File(rutaFichDestino);//objeto con la ruta destino.
        BufferedInputStream lectorFichero;//flujo buffer de lectura .
        BufferedOutputStream escritorFichero;//buffer de escritura.
        try{
            lectorFichero=new BufferedInputStream(new FileInputStream(ficheroOrigen));//Inicializa el buffer de lectura con un FileInputStream
            escritorFichero=new BufferedOutputStream(new FileOutputStream(ficheroDestino));//Inicializa el buffer de escritura con un FileOutputStream
            int bytes;
            
            String read, inv;

            //int[] intRead, invInt;
            int[] intRead,intAux;
            intRead = new int[1];
            intAux = new int[1];
            long conv;
            intAux[0]=0;
            intRead[0]=0;
            do{    
            intAux=intRead;
            intRead=recoverNext(1,lectorFichero, escritorFichero);
            
            }while(!(intRead[0]==0xD9&&intAux[0]==0XFF));

            System.out.println("Primeros dos: ");
            printArrayInt(intRead);
            System.out.println("");


            printArrayIntInHex(intRead);
            System.out.println("");


            intRead = new int[3];

            intRead=recoverNext(3,lectorFichero, escritorFichero);

            invInt = new int[intRead.length];
            invInt=reverseInt(intRead);

            System.out.println("Siguientes tres: ");
            printArrayInt(intRead);
            System.out.println("");

            printArrayIntInHex(intRead);
            System.out.println("");

            System.out.println("Siguientes tres invertidos: ");
            printArrayInt(invInt);
            System.out.println("");

            printArrayIntInHex(invInt);
            System.out.println("");


            //read=recoverNext(2,lectorFichero, escritorFichero);

            //read=recoverNext(2,lectorFichero, escritorFichero);


            lectorFichero.close();
            escritorFichero.close();

        }
        catch(FileNotFoundException e){
            e.printStackTrace();

        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
*/
    static int[] recoverNext(int nextBytes,BufferedInputStream lectorFichero, BufferedOutputStream escritorFichero){
        int[] bytesRecovered= new int[nextBytes];
        String byteRead="";
        int count=0;
        int byteRecovered;
        try{
            if(nextBytes>0){
                while(count<nextBytes){
                    if((byteRecovered=lectorFichero.read())!=-1){
                       //System.out.println("Leido: "+byteRecovered);

                       bytesRecovered[count]=byteRecovered;
                       writeToFile(escritorFichero, byteRecovered);
                    }
                    count++;
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return(bytesRecovered);
    }

    static int[] reverseInt(int[] bytes){
        int[] revString= new int[bytes.length];
        int index=0;
        for (int x=bytes.length-1;x>=0;x--){
            revString[index++] = bytes[x];
        }
        return(revString);
    }

    static void printArrayInt(int[] intArray){
        for (int x=0; x<intArray.length;x++){
            System.out.print(intArray[x]+" ");
        }
    }

    static void printArrayIntInHex(int[] intArray){
        for (int x=0; x<intArray.length;x++){
            System.out.print(Integer.toHexString(intArray[x])+" ");
        }
    }

    static long StringToInt(String bytes){
        long value=0;

        value=Long.parseLong(bytes, 16);

        return(value);
    }

    static void writeToFile(BufferedOutputStream escritorFichero, int bytes){
        try{
            escritorFichero.write(bytes);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

}
