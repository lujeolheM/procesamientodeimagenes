/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImageProces;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.Buffer;
import javax.imageio.ImageIO;



/**
 *
 * @author Geek
 */
public class TutorialRedim {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        String ruta=" ";
        if(args.length!=0){
            ruta=args[0];
            
        }
            
        else{
            ruta="./entrada.jpg";
        }
            
        System.out.println("Ruta del archivo: "+ruta);
        Imagen obj = new Imagen(ruta);
        obj.copiarImagen();
        obj.filtroSepia();
        //obj.binarizarImagenAR();
        //obj.binarizarImagenBN();
        BufferedImage img = obj.imprimirImagen();
        ImageIO.write(img, "jpg", new File("salidaSepia.jpg"));
        
        
        Imagen obj3 = new Imagen(ruta);
        obj3.binarizarImagenAR();
        img = obj3.imprimirImagen();
        ImageIO.write(img, "jpg", new File("salidaBAR.jpg"));
        
        Imagen obj2 = new Imagen(ruta);
        obj2.binarizarImagenBN();
        img = obj2.imprimirImagen();
        ImageIO.write(img, "jpg", new File("salidaBBN.jpg"));
        
        
        
    }
}
